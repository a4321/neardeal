package com.iagrup.neardeal;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;

import com.iagrup.neardeal.adapter.CartItemAdapter;
import com.iagrup.neardeal.database.model.Cart;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmResults;

public class CartActivity extends AppCompatActivity {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.btn_lanjut)
    Button btnLanjut;

    //deklarasi List<Cart> untuk menyimpan value dari Object Realm
    List<Cart> cartList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        ButterKnife.bind(this);

        //deklarasi CartItemAdapter untuk menggunakan adapter
        CartItemAdapter adapter = new CartItemAdapter(this,cartList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        //mengambil data dari realm
        Realm realm = Realm.getDefaultInstance();
        //deklari RealmResult<Cart> untuk menyimpan value dari hasil query Realm
        RealmResults<Cart> cartRealmResults = realm.where(Cart.class).findAll();
        //memproses hasil realmResult memasukan hasil query kedalam carList
//        for (int i = 0; i < cartRealmResults.size(); i++){
//            Cart cart = cartRealmResults.get(i);
//            if (cart != null){
//                cartList.add(realm.copyFromRealm(cart));
//            }
//        }

        //Testing new looping
        for (Cart cart : cartRealmResults){
            if (cart != null){
                cartList.add(realm.copyFromRealm(cart));
            }
        }

        adapter.notifyDataSetChanged();
        realm.close();

    }

    @OnClick(R.id.btn_lanjut)
    public void onViewClicked() {
        startActivity(new Intent(CartActivity.this, CheckoutActivity.class));
    }
}
