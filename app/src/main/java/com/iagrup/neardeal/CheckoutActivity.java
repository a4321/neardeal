package com.iagrup.neardeal;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.iagrup.neardeal.api.ApiClient;
import com.iagrup.neardeal.api.ApiEndPoint;
import com.iagrup.neardeal.database.model.Cart;
import com.iagrup.neardeal.response.CheckoutResponse;
import com.iagrup.neardeal.utils.PopupUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckoutActivity extends AppCompatActivity {

    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.et_no_hp)
    EditText etNoHp;
    @BindView(R.id.et_address)
    EditText etAddress;
    @BindView(R.id.btn_submit)
    Button btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_submit)
    public void onViewClicked() {
        checkout();
    }

    //Membuat method checkout untuk POST data ke server
    private void checkout(){
        //Mengambil value dari EditText
        String name = etName.getText().toString().trim();
        String noHp = etNoHp.getText().toString().trim();
        String address = etAddress.getText().toString().trim();

        //Mengambil data dari Realm
        final Realm realm = Realm.getDefaultInstance();
        final RealmResults<Cart> realmResults = realm.where(Cart.class).findAll();

        //Membuat perulangan untuk mengambil productId dan prices
        List<String> productIds = new ArrayList<>();
        List<String> prices = new ArrayList<>();
        //Melakukan perulangan untuk memasukkan productId dan price kedalam
        //arraylist
        for (Cart cart : realmResults){
            productIds.add(cart.getProductId());
            prices.add(String.valueOf(cart.getPrice()));
        }
        realm.close();

        //Menampilkan loading
        PopupUtil.showLoading(this, "Mohon tunggu",
                "Memesan pesanan anda");

        //konversi ArrayList, menjadi String
        String productIdString = TextUtils.join(",",productIds);
        String priceString = TextUtils.join(",", prices);

        //Melakukan POST ke server
        ApiEndPoint apiEndPoint = ApiClient.getClient(this).create(ApiEndPoint.class);
        Call<CheckoutResponse> call = apiEndPoint.checkout(name, noHp, address, productIdString,
                priceString);
        call.enqueue(new Callback<CheckoutResponse>() {
            @Override
            public void onResponse(Call<CheckoutResponse> call, Response<CheckoutResponse> response) {
                PopupUtil.dismissDialog();
                CheckoutResponse checkoutResponse = response.body();
                if (checkoutResponse != null && checkoutResponse.getSuccess()){
                    //Apabila berhasil melakukan POST data ke server, tampilkan pesan
                    Toast.makeText(CheckoutActivity.this, "Produk berhasil dipesan", Toast.LENGTH_SHORT).show();
                    //Pindah activity ke MainActivity
                    Intent intent = new Intent(CheckoutActivity.this, MainActivity.class);
                    //Menghapus Activity yang berjalan di background
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();

                    //Menghapus data pada Realm
                    Realm realmDelete = Realm.getDefaultInstance();
                    realmDelete.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            RealmResults<Cart> realmResultsDelete = realm.where(Cart.class).findAll();
                            realmResultsDelete.deleteAllFromRealm();
                        }
                    });
                    realmDelete.close();
                }
            }

            @Override
            public void onFailure(Call<CheckoutResponse> call, Throwable t) {

            }
        });
    }
}
