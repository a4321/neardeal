package com.iagrup.neardeal;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.iagrup.neardeal.api.ApiClient;
import com.iagrup.neardeal.api.ApiEndPoint;
import com.iagrup.neardeal.response.LoginResponse;
import com.iagrup.neardeal.utils.PopupUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.et_username)
    EditText etUsername;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.btn_login)
    Button btnLogin;

    //Membuat variable String untuk menyimpan value dari EditText
    String username;
    String password;

    //Deklarasi LoginResponse untuk menyimpan value kiriman dari server
    LoginResponse loginResponse;

    //membuat variable string loggenUsername dan loggenPassword untuk menyimpan username & password
    //kedalam device android (session)
    String loggedUsername;
    String loggedPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        //Mengambil status isLoggidIn dari sharedPreference
        SharedPreferences preferences = getSharedPreferences("login",MODE_PRIVATE);
        boolean isLoggedIn = preferences.getBoolean("isLoggedIn",false);

        //mengecek status user apakan sudah login atau belum ?
        if (isLoggedIn){
            //apabila status sudah login langsung melakukan intent ke mainactivity
            startActivity(new Intent(LoginActivity.this,MainActivity.class));
            //debug only
            Toast.makeText(this, "User sudah Login, skip login activity", Toast.LENGTH_SHORT).show();
            //Destroy LoginActivity
            finish();
        }

    }

    @OnClick(R.id.btn_login)
    public void onViewClicked() {

        //menampilkan loading menu
        PopupUtil.showLoading(this,"Mohon Tunggu","Login");
        //Mengambil value username & password
        username = etUsername.getText().toString().trim();
        password = etPassword.getText().toString();

        //memanggil server
        ApiEndPoint apiEndPoint = ApiClient.getClient(this).create(ApiEndPoint.class);
        Call<LoginResponse> call = apiEndPoint.login(username, password);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                //menutup popo loading
                PopupUtil.dismissDialog();
                //mengambil response serve
                loginResponse =  response.body();
                //mengecek apabila respon tidak null & succes == true
                if (loginResponse != null & loginResponse.getSuccess()){
                    //apabila valid maka simpan ke dalam SharedPreference
                    SharedPreferences preferences = getSharedPreferences("login",MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("username", username);
                    editor.putString("password", password);
                    //Menyimpan status user => loged in
                    editor.putBoolean("isLoggedIn",true);
                    editor.apply();

                    //menjalankan intent
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    finish();
                    //apabila succes == flase, maka tampilan toas username password salah
                }else if (loginResponse != null && !loginResponse.getSuccess()){
                    Toast.makeText(LoginActivity.this, "Username atau Password Salah", Toast.LENGTH_SHORT).show();
                }else {
                    //apabila response == null tam[ilkan toast warning
                    Toast.makeText(LoginActivity.this, "Response is Null", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {

            }
        });

    }
}
