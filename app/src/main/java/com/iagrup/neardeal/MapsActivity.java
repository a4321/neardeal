package com.iagrup.neardeal;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.iagrup.neardeal.response.Store;

import java.lang.reflect.Type;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    private double lat;
    private double lng;

    //membuat variable string untuk menyimpan storejson yang diambil dari shared preference
    private  String storeJson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //mengambil kiriman value dari mainactivity dan disimpan dari  vvariable lat&lng
        lat = getIntent().getDoubleExtra("lat",0);
        lng = getIntent().getDoubleExtra("lng",0);

        //memanggil sharedpreference dengan nama package project
        SharedPreferences preferences = getSharedPreferences(
                "com.iagrup.neardeal",MODE_PRIVATE);
        //mengambil storeJson dari sharedpreference
        storeJson = preferences.getString("storeJson","");
        //logging
        Log.d("MapsActivity","storeJson"+storeJson);

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng userLocation = new LatLng(lat, lng);
        mMap.addMarker(new MarkerOptions().position(userLocation).title("Your Location"));

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(userLocation,15));

        //memberikan animasi pada camera
        mMap.animateCamera(CameraUpdateFactory.zoomIn());
        mMap.animateCamera(CameraUpdateFactory.zoomTo(10),2000,null);

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(userLocation)
                .zoom(17)
                .bearing(90)
                .tilt(30)
                .build();

        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));


        //Panggil method storemarker
        makeStoreMarker();
    }

    private void makeStoreMarker(){
        //melakukan keonversi json ke gson
        Gson gson = new Gson();
        Type listOfObject =  new TypeToken<List<Store>>(){}.getType();
        //membuat variable list<store> untuk menyimpan value ArrayList dari hasil konversi Json ke Gson
        List<Store> storeList = gson.fromJson(storeJson,listOfObject);

        Log.d("MapsActivity","storeList.size" +storeList.size());
        //mengecek apabila storelist tidak null
        if (storeList != null){
            //lakukan perulangan untuk menampilkan marker berdasarkan lat & lng
            for (int i=0; i < storeList.size();i++){
                //membuat store mendapatkan data array sesuai banyak i (array)
                Store store = storeList.get(i);
                //membuat variableb latlng
                LatLng latLng = new LatLng(Double.parseDouble(store.getLat()),
                        Double.parseDouble(store.getLng()));

                //membuat marker sesuai dengan data dari array store
                    mMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .title(store.getName())
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.store)));
            }
        }

    }
}
