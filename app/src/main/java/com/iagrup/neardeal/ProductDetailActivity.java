package com.iagrup.neardeal;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.iagrup.neardeal.api.ApiClient;
import com.iagrup.neardeal.api.ApiEndPoint;
import com.iagrup.neardeal.database.model.Cart;
import com.iagrup.neardeal.response.ProductDetailResponse;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductDetailActivity extends AppCompatActivity {


    @BindView(R.id.image_view)
    ImageView imageView;
    @BindView(R.id.profile_image)
    CircleImageView profileImage;
    @BindView(R.id.tv_store_name)
    TextView tvStoreName;
    @BindView(R.id.tv_harga)
    TextView tvHarga;
    @BindView(R.id.tv_description)
    TextView tvDescription;
    @BindView(R.id.btn_buy)
    Button btnBuy;
    @BindView(R.id.tv_price_old)
    TextView tvPriceOld;
    @BindView(R.id.tv_price)
    TextView tvPrice;

    //deklarasi variable productId untuk menyimpan value kiriman dari Fragment
    private String productId;

    private ProductDetailResponse productDetailResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        ButterKnife.bind(this);

        //mengambil kiriman dari productId
        productId = getIntent().getStringExtra("productId");

        loadProductDetail();
    }

    @OnClick(R.id.btn_buy)
    public void onViewClicked() {
        buyProduct();
    }

    //membuat method buyProduct untuk menyimpan product yang dibeli kedalam Realm
    private  void buyProduct(){
        //inisiasi Realm
        Realm realm = Realm.getDefaultInstance();
        //mengambil data product
        final String name = productDetailResponse.getProductdetails().getName();
        final double price = Double.parseDouble(productDetailResponse.getProductdetails().getPrice());
        final String photo = productDetailResponse.getProductdetails().getPhoto();

        //melakukan insert database
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                //mengambil seluruh data yang dimiliki oleh object Card,
                RealmResults<Cart> carts = realm.where(Cart.class)
                        //melakukan sort by id, dengan format urutsan Desc.
                .sort("id", Sort.DESCENDING).findAll();
                //membuat variable lastId
                int lastId = 0;

                //Mengecek kondisi apabila hasil data array pada object Cart > 0 (ada)
                //maka ubah nilai last id menjadi id terakhir pada object Cart
                if (carts.size() > 0){
                    lastId = carts.first().getId();
                }
                //memasukan data kedalam object Realm
                Cart cart = new Cart();
                //pengganti auto increament
                cart.setId(lastId + 1);
                cart.setProductId(productId);
                cart.setProductName(name);
                cart.setPrice(price);
                cart.setPhoto(photo);

                //melakukan eksekusi realm, untuk memasukan data ke Cart ke dalam object Cart
                try{
                    realm.copyToRealm(cart);
                    Toast.makeText(ProductDetailActivity.this, "Produk Berhasil Ditambah", Toast.LENGTH_SHORT).show();
                    finish();
                }catch (Exception e ){
                    Log.e("ProductDetailActivity", "Failed to save Realm Data" + e);
                    Toast.makeText(ProductDetailActivity.this, "Pembelian Produk Gagal", Toast.LENGTH_SHORT).show();
                }

            }
        });
        realm.close();
    }

    //membuat method loadProductDetail untuk mengambil data dari serveer
    private void loadProductDetail(){
        ApiEndPoint apiEndPoint = ApiClient.getClient(this).create(ApiEndPoint.class);
        Call<ProductDetailResponse> call = apiEndPoint.getProductDetail(productId);
        call.enqueue(new Callback<ProductDetailResponse>() {
            @Override
            public void onResponse(Call<ProductDetailResponse> call, Response<ProductDetailResponse> response) {
                //set data view sesuai dengan data dari serve
                productDetailResponse = response.body();
                if (productDetailResponse != null && productDetailResponse.getSuccess()){
                    Picasso.get().load(productDetailResponse.getProductdetails().getStorePhoto()).into(profileImage);
                    Picasso.get().load(productDetailResponse.getProductdetails().getPhoto()).into(imageView);
                    tvStoreName.setText(productDetailResponse.getProductdetails().getStoreName());
                    tvPriceOld.setText(productDetailResponse.getProductdetails().getPrice());
                    setTitle(productDetailResponse.getProductdetails().getName());






                }
            }

            @Override
            public void onFailure(Call<ProductDetailResponse> call, Throwable t) {

            }
        });
    }
}
