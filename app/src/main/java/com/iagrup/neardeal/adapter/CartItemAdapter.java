package com.iagrup.neardeal.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.iagrup.neardeal.R;
import com.iagrup.neardeal.database.model.Cart;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class CartItemAdapter extends RecyclerView.Adapter<CartItemAdapter.MyViewHolder> {

    private List<Cart> cartList;
    private Context context;
    private LayoutInflater inflater;

    //deklarasi interface onitemclicklistener
    private CartItemAdapter.OnItemClickListener onItemClickListener;

    //Constructor CartItemAdapter untuk mendapatkan value dari paramater
    //dan memberikan value tersebut ke valiabel lokal
    public CartItemAdapter(Context context, List<Cart> cartList){
        this.cartList = cartList;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }


    //override oncreateviewholder untuk menampilkan item layout yang telah d buat
    @Override
    public CartItemAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.item_cart,parent,false);

        return new CartItemAdapter.MyViewHolder(view) ;
    }

    //override onbindviewholder untuk melakukan set data pada masing2 view
    @Override
    public void onBindViewHolder(CartItemAdapter.MyViewHolder holder, int position) {
        final Cart cart = cartList.get(position);
        holder.tvName.setText(cart.getProductName());

        Locale localID = new Locale("in","ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localID);


        holder.tvHarga.setText(formatRupiah.format(cart.getPrice()));

        //set image menggunakan picasso
        Picasso.get().load(cart.getPhoto()).into(holder.imageView);

        //membuat container dapat diklik
        if (onItemClickListener != null){
            holder.container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemClick(cart.getProductId());
                }
            });
        }

    }

    //menentukan seberapa banyak iptem layout dilooping
    @Override
    public int getItemCount() {
        return cartList.size();
    }

    //membuat class viewholder untuk melakukan casting view
    public class MyViewHolder extends RecyclerView.ViewHolder{

        ImageView imageView;
        TextView tvName;
        TextView tvHarga;
        FrameLayout container;

        public MyViewHolder(View view){
            super(view);

            //melakukan casting view
            imageView = view.findViewById(R.id.img_view);
            tvName = view.findViewById(R.id.tv_nama_barang);
            tvHarga = view.findViewById(R.id.tv_harga);
            container = view.findViewById(R.id.frm_cont);
        }
    }

    //membuat interface onitemclicklistener
    public interface  OnItemClickListener {
        void onItemClick(String cartId);
    }

    //membuat method setlistener untuk memberikan
    public void setListener(CartItemAdapter.OnItemClickListener listener){
        onItemClickListener = listener;
    }

}
