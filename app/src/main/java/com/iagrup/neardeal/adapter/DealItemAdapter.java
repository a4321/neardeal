package com.iagrup.neardeal.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.iagrup.neardeal.R;
import com.iagrup.neardeal.response.Deal;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class DealItemAdapter extends RecyclerView.Adapter<DealItemAdapter.MyViewHolder> {

    private List<Deal> dealList;
    private Context context;
    private LayoutInflater inflater;

    //deklarasi interface onitemclicklistener
    private DealItemAdapter.OnItemClickListener onItemClickListener;

    //Constructor DealItemAdapter untuk mendapatkan value dari paramater
    //dan memberikan value tersebut ke valiabel lokal
    public DealItemAdapter(Context context, List<Deal> dealList){
        this.dealList = dealList;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }


    //override oncreateviewholder untuk menampilkan item layout yang telah d buat
    @Override
    public DealItemAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.item_deal,parent,false);

        return new DealItemAdapter.MyViewHolder(view) ;
    }

    //override onbindviewholder untuk melakukan set data pada masing2 view
    @Override
    public void onBindViewHolder(DealItemAdapter.MyViewHolder holder, int position) {
        final Deal deal = dealList.get(position);
        holder.tvName.setText(deal.getName());

        //membuat format rupiah
        Locale localeID = new Locale("in","ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);

        holder.tvHargaLama.setText(formatRupiah.format(Long.parseLong(deal.getPrice())));
        //Strike True
        holder.tvHargaLama.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);

        //set image menggunakan picasso
        Picasso.get().load(deal.getPhoto()).into(holder.imageView);

        //menghitung harga baru berdasarkan discount dari deal
        int diskon = Integer.parseInt(deal.getDiscount());
        int hargaLama = Integer.parseInt(deal.getPrice());
        double priceTotal = hargaLama - (hargaLama * diskon / 100);
        holder.tvHargaBaru.setText(formatRupiah.format(priceTotal));

        //mengambil nilai selilih tanggal diskon
        int selisih = Integer.parseInt(deal.getSelisihHari());
        //kondisi apabila selisih < 0
        if (selisih < 0){
            holder.tvTglDiskon.setText("Diskon Telah Habis");
        }else if (selisih == 0){
            //apabila selisih == 0 maka tampilan diskon sedang berlangsung
            holder.tvTglDiskon.setText("Diskon Sedang Berlangsung");
        }else {
            //apabila selisih lebih > 0 tampilkan jumlah hari diskon
            holder.tvTglDiskon.setText("Diskon Dimulai " +selisih+ " Hari Lagi");
        }


        //membuat container dapat diklik
        if (onItemClickListener != null){
            holder.container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemClick(deal.getProductId());
                }
            });
        }

    }

    //menentukan seberapa banyak iptem layout dilooping
    @Override
    public int getItemCount() {
        return dealList.size();
    }

    //membuat class viewholder untuk melakukan casting view
    public class MyViewHolder extends RecyclerView.ViewHolder{

        ImageView imageView;
        TextView tvName;
        TextView tvHargaBaru;
        TextView tvHargaLama;
        TextView tvTglDiskon;
        FrameLayout container;

        public MyViewHolder(View view){
            super(view);

            //melakukan casting view
            imageView = view.findViewById(R.id.img_view);
            tvName = view.findViewById(R.id.tv_nama_barang);
            tvHargaLama = view.findViewById(R.id.tv_harga_lama);
            tvHargaBaru = view.findViewById(R.id.tv_harga_baru);
            tvTglDiskon = view.findViewById(R.id.tv_tgl_diskon);
            container = view.findViewById(R.id.frm_cont);
        }
    }

    //membuat interface onitemclicklistener
    public interface  OnItemClickListener {
        void onItemClick(String productId);
    }

    //membuat method setlistener untuk memberikan
    public void setListener(DealItemAdapter.OnItemClickListener listener){
        onItemClickListener = listener;
    }

}
