package com.iagrup.neardeal.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.iagrup.neardeal.R;
import com.iagrup.neardeal.response.Product;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ProductItemAdapter extends RecyclerView.Adapter<ProductItemAdapter.MyViewHolder> {

    private List<Product> productList;
    private Context context;
    private LayoutInflater inflater;

    //deklarasi interface onitemclicklistener
    private ProductItemAdapter.OnItemClickListener onItemClickListener;

    //Constructor ProductItemAdapter untuk mendapatkan value dari paramater
    //dan memberikan value tersebut ke valiabel lokal
    public ProductItemAdapter(Context context, List<Product> productList){
        this.productList = productList;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }


    //override oncreateviewholder untuk menampilkan item layout yang telah d buat
    @Override
    public ProductItemAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.item_product,parent,false);

        return new ProductItemAdapter.MyViewHolder(view) ;
    }

    //override onbindviewholder untuk melakukan set data pada masing2 view
    @Override
    public void onBindViewHolder(ProductItemAdapter.MyViewHolder holder, int position) {
        final Product product = productList.get(position);
        holder.tvName.setText(product.getName());
        holder.tvHarga.setText(product.getPrice());

        //set image menggunakan picasso
        Picasso.get().load(product.getPhoto()).into(holder.imageView);

        //membuat container dapat diklik
        if (onItemClickListener != null){
            holder.container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemClick(product.getId());
                }
            });
        }

    }

    //menentukan seberapa banyak iptem layout dilooping
    @Override
    public int getItemCount() {
        return productList.size();
    }

    //membuat class viewholder untuk melakukan casting view
    public class MyViewHolder extends RecyclerView.ViewHolder{

        ImageView imageView;
        TextView tvName;
        TextView tvHarga;
        FrameLayout container;

        public MyViewHolder(View view){
            super(view);

            //melakukan casting view
            imageView = view.findViewById(R.id.img_view);
            tvName = view.findViewById(R.id.tv_nama_barang);
            tvHarga = view.findViewById(R.id.tv_harga);
            container = view.findViewById(R.id.frm_cont);
        }
    }

    //membuat interface onitemclicklistener
    public interface  OnItemClickListener {
        void onItemClick(String productId);
    }

    //membuat method setlistener untuk memberikan
    public void setListener(ProductItemAdapter.OnItemClickListener listener){
        onItemClickListener = listener;
    }

}
