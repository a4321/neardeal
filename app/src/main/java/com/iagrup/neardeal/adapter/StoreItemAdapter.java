package com.iagrup.neardeal.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.iagrup.neardeal.R;
import com.iagrup.neardeal.response.Store;
import com.squareup.picasso.Picasso;

import java.util.List;

//membuat class adapter untuk menampilkan data store
//menggunakan recycler view
public class StoreItemAdapter extends RecyclerView.Adapter<StoreItemAdapter.MyViewHolder> {

    private List<Store> storeList;
    private Context context;
    private LayoutInflater inflater;

    //deklarasi interface onitemclicklistener
    private OnItemClickListener onItemClickListener;

    //Constructor StoreItemAdapter untuk mendapatkan value dari paramater
    //dan memberikan value tersebut ke valiabel lokal
    public StoreItemAdapter(Context context, List<Store> storeList){
        this.storeList = storeList;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }


    //override oncreateviewholder untuk menampilkan item layout yang telah d buat
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.item_store,parent,false);

        return new MyViewHolder(view) ;
    }

    //override onbindviewholder untuk melakukan set data pada masing2 view
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Store store = storeList.get(position);
        holder.tvName.setText(store.getName());

        //set image menggunakan picasso
        Picasso.get().load(store.getPhoto()).into(holder.imageView);

        //membuat container dapat diklik
        if (onItemClickListener != null){
            holder.container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemClick(store.getId());
                }
            });
        }

    }

    //menentukan seberapa banyak iptem layout dilooping
    @Override
    public int getItemCount() {
        return storeList.size();
    }

    //membuat class viewholder untuk melakukan casting view
    public class MyViewHolder extends RecyclerView.ViewHolder{

        ImageView imageView;
        TextView tvName;
        TextView tvDetail;
        FrameLayout container;

        public MyViewHolder(View view){
            super(view);

            //melakukan casting view
            imageView = view.findViewById(R.id.img_gambar);
            tvName = view.findViewById(R.id.tv_nama_toko);
            tvDetail = view.findViewById(R.id.tv_name_detail);
            container = view.findViewById(R.id.frm_layout);
        }
    }

    //membuat interface onitemclicklistener
    public interface  OnItemClickListener {
        void onItemClick(String storeId);
    }

    //membuat method setlistener untuk memberikan
    public void setListener(OnItemClickListener listener){
        onItemClickListener = listener;
    }
}
