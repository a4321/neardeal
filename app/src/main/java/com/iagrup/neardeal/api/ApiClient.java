package com.iagrup.neardeal.api;

import android.content.Context;

import com.franmontiel.persistentcookiejar.ClearableCookieJar;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    private static final String BASE_URL = "http://192.168.1.9/neardeal/";
    private static Retrofit retrofit;

    //membuat method API client untuk memanggil koneksi server
    public static Retrofit getClient(Context context){
        if (retrofit == null){
            retrofit =  new Retrofit.Builder()
                            .baseUrl(BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .client(getHeader(context))
                            .build();
        }
        return retrofit;
    }

    //Membuat method getHeader dari OkHttpClient untuk mengambil
    //Log process htpp, Cache dan Cookie
    private static OkHttpClient getHeader(Context context){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        //menambahkan cookieJar untuk mentimpan cookie pada client
        ClearableCookieJar cookieJar = new PersistentCookieJar(new SetCookieCache(),
                new SharedPrefsCookiePersistor(context));

        return new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                //mengaktifkan cookieJar
                .cookieJar(cookieJar)
                .build();
    }


}
