package com.iagrup.neardeal.api;

import com.iagrup.neardeal.response.CheckoutResponse;
import com.iagrup.neardeal.response.DealResponse;
import com.iagrup.neardeal.response.LoginResponse;
import com.iagrup.neardeal.response.LogoutResponse;
import com.iagrup.neardeal.response.ProductDetailResponse;
import com.iagrup.neardeal.response.ProductResponse;
import com.iagrup.neardeal.response.StoreResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiEndPoint {
    @GET("get_store.php")
    Call<StoreResponse> getStore(@Query("lat") double lat,
                                 @Query("lng") double lng
                                 );

    @GET("get_product.php")
    Call<ProductResponse> getProduct(@Query("store_id")String storeId);

    @GET("get_deal.php")
    Call<DealResponse> getDeal(@Query("store_id") String storeId);

    @GET("get_product_detail.php")
    Call<ProductDetailResponse>getProductDetail(@Query("product_id")
                                                String productId);

    //membuat method login memanggil API login.php
    @FormUrlEncoded
    @POST("login.php")
    Call<LoginResponse> login (@Field("username") String username,
                                @Field("password") String password );

    //membuat method checkout untuk API checkout.php
    @FormUrlEncoded
    @POST("checkout.php")
    Call<CheckoutResponse> checkout (@Field("name") String name,
                                     @Field("no_hp") String no_hp,
                                     @Field("address") String address,
                                     @Field("productIds") String productIds,
                                     @Field("prices") String prices
                                     );
    //membuat method logout untuk memanggil API LOGOTU
    @GET("logout.php")
    Call<LogoutResponse> logout();

}
