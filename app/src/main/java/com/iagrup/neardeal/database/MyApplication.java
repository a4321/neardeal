package com.iagrup.neardeal.database;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class MyApplication extends android.app.Application {
    @Override
    public void onCreate(){
        super.onCreate();

        //mengaktifkan Realm
        Realm.init(this);
        //membuat konfigurasi realm
        RealmConfiguration configuration = new RealmConfiguration.Builder()
                //Nama database
                .name("near_deal")
                //versi database
                .schemaVersion(1)
                .build();

        //mengaktifkan konfigurasi Realm
        Realm.setDefaultConfiguration(configuration);
    }
}
