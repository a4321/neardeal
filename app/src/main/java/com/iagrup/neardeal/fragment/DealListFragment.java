package com.iagrup.neardeal.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.iagrup.neardeal.R;
import com.iagrup.neardeal.adapter.DealItemAdapter;
import com.iagrup.neardeal.api.ApiClient;
import com.iagrup.neardeal.api.ApiEndPoint;
import com.iagrup.neardeal.response.Deal;
import com.iagrup.neardeal.response.DealResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class DealListFragment extends Fragment {

    //deklarasi storeID untuk menyimpan value kiriman data dari ProductActivity
    private String storeId;

    //deklarasi DealItemAdapter agar dapat memanggil class adapter
    private DealItemAdapter adapter;

    //deklarasi dealList untuk menyimpan value yang diambil dari serve
    private List<Deal> dealList;

    public DealListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_deal_list, container, false);

        //mengambil kiriman data menggunakan bundel
        Bundle bundle = getArguments();
        if (bundle != null){
            storeId = bundle.getString("storeId");
        }
        //inisiasi dealList
        dealList = new ArrayList<>();
        //inisiasi adapter
        adapter = new DealItemAdapter(getActivity(),dealList);

        //casing RecycleView
        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),2));
        recyclerView.setAdapter(adapter);

        loadDeal();

        return view;

    }

    //membuat method loadDeal untuk load data ke serve menggunakan retrofit
    private void loadDeal() {
        ApiEndPoint apiEndPoint = ApiClient.getClient(getActivity()).create(ApiEndPoint.class);
        retrofit2.Call<DealResponse> call = apiEndPoint.getDeal(storeId);
        call.enqueue(new Callback<DealResponse>() {
            @Override
            public void onResponse(retrofit2.Call<DealResponse> call, Response<DealResponse> response) {
                DealResponse dealResponse =  response.body();
                if (dealResponse != null && dealResponse.getSuccess()){
                    dealList.addAll(dealResponse.getDeal());
                    if (getActivity() != null) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                adapter.notifyDataSetChanged();
                            }
                        });
                    }
                }
            }

            @Override
            public void onFailure(retrofit2.Call<DealResponse> call, Throwable t) {

            }
        });
    }

}
