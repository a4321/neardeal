package com.iagrup.neardeal.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.iagrup.neardeal.ProductDetailActivity;
import com.iagrup.neardeal.R;
import com.iagrup.neardeal.adapter.ProductItemAdapter;
import com.iagrup.neardeal.api.ApiClient;
import com.iagrup.neardeal.api.ApiEndPoint;
import com.iagrup.neardeal.response.Product;
import com.iagrup.neardeal.response.ProductResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductListFragment extends Fragment {

    //deklarasi string untuk menyimpan value kririman dari productactivity
    private String storeId;

    //deklarasi variable list untuk menyimpan value list product
    private List<Product> productsList;

    //deklarasi productItemAdapter
    private ProductItemAdapter productItemAdapter;


    public ProductListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_product_list, container, false);

        //pengambilan balue kiriman dari product activity
        Bundle bundle = getArguments();
        if (bundle != null){
            storeId = bundle.getString("storeId");
            //logging
            Log.d("ProductActivity","storeId " +storeId);
        }

        //inisiasi
        productsList = new ArrayList<>();
        productItemAdapter =  new ProductItemAdapter(getActivity(),productsList);

        //deklarasi & casting recyclerView
        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);

        //membuat GridLayoutManager pada recycleView
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),2));
        //mengaktifkan adapter pada recycle view
        recyclerView.setAdapter(productItemAdapter);
        //mengaktifkan click listener untuk adapter sambil mengirim data productId
        productItemAdapter.setListener(new ProductItemAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String productId) {
                //apabila item diklik, maka pindah ke productDetailActivity
                Intent intent = new Intent(getActivity(), ProductDetailActivity.class);
                //mengirim product id
                intent.putExtra("productId",productId);
                startActivity(intent);

                //logging
                Log.d("ProductListFragment","productId" +productId);
            }
        });

        loadProduct();

        return view;
    }

    //mengambil data dari product dari server
    private  void loadProduct(){
        ApiEndPoint apiEndPoint = ApiClient.getClient(getActivity())
                .create(ApiEndPoint.class);
        Call<ProductResponse> call = apiEndPoint.getProduct(storeId);
        call.enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                ProductResponse productResponse = response.body();
                if (productResponse != null && productResponse.getSuccess()){
                    productsList.addAll(productResponse.getProduct());
                    if (getActivity() != null) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                productItemAdapter.notifyDataSetChanged();
                            }
                        });


                    }
                }else {
                    Toast.makeText(getActivity(), "Response is Null", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ProductResponse> call, Throwable t) {

            }
        });
    }

}
