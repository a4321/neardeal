package com.iagrup.neardeal.fragment;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.iagrup.neardeal.ProductActivity;
import com.iagrup.neardeal.R;
import com.iagrup.neardeal.adapter.StoreItemAdapter;
import com.iagrup.neardeal.api.ApiClient;
import com.iagrup.neardeal.api.ApiEndPoint;
import com.iagrup.neardeal.response.Store;
import com.iagrup.neardeal.response.StoreResponse;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class StoreListFragment extends Fragment {

    //deklarasi StoreItemAdapter & List<Store>
    private StoreItemAdapter storeItemAdapter;
    private List<Store> storeList;

    //deklarasi variable lat & long
    private double lat;
    private double lng;


    public StoreListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_store_list, container, false);

        //mengambil kiriman bundle dari mainactivity
        Bundle bundle =getArguments();
        lat = bundle.getDouble("lat",0);
        lng = bundle.getDouble("lng",0);


        //inisisasi class yanag telah di dekalarasi
        storeList = new ArrayList<>();
        storeItemAdapter = new StoreItemAdapter(getActivity(),storeList);

        //deklarasi & casting recycleview
        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        //menentukan layoutManager untuk recycleview
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        //Mengaktifkan recyccleView
        recyclerView.setAdapter(storeItemAdapter);

        loadStore();

        //membuat recycle dapat diclick
        storeItemAdapter.setListener(new StoreItemAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String storeId) {
                //memanggil intent untuk berpindah activity
                Intent intent = new Intent(getActivity(), ProductActivity.class);
                intent.putExtra("storeId",storeId);
                startActivity(intent);
                //loging
                Log.d("StoreListFragment","storeId" +storeId);
            }
        });

        return view;
    }

    private  void loadStore(){
        ApiEndPoint apiEndPoint = ApiClient.getClient(getActivity())
                .create(ApiEndPoint.class);
        Call<StoreResponse> call = apiEndPoint.getStore(lat,lng);
        call.enqueue(new Callback<StoreResponse>() {
            @Override
            public void onResponse(Call<StoreResponse> call, Response<StoreResponse> response) {
                StoreResponse storeResponse = response.body();
                if (storeResponse != null && storeResponse.getSuccess()){
                    storeList.addAll(storeResponse.getStore());
                    if (getActivity() != null) {


                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                storeItemAdapter.notifyDataSetChanged();

                                //melakukan convert List<Store> menjadi json
                                Gson gson = new Gson();
                                Type listOfObject = new TypeToken<List<Store>>() {}.getType();
                                //membuat variable baru storeJson yang berisikan hasil konversi
                                //object store menjadi json
                                String storeJson = gson.toJson(storeList, listOfObject);
                                //logging storeJson
                                Log.d("StoreListFragment", "storeJson" + storeJson);

                                //menyimpan storeJson ke dalam sharedPreference (versi web : session)
                                //membuat variable sharedpreference yang berisikan data dari nama package project
                                SharedPreferences sharedPreferences = getActivity()
                                        .getSharedPreferences("com.iagrup.neardeal", Context.MODE_PRIVATE);
                                //membuat variable editor agar dapat memasukan data ke sharedpreference
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                //memasukan data menggunakan editor
                                editor.putString("storeJson",storeJson);
                                //save
                                editor.apply();

                            }
                        });
                    }
                }else {
                    Toast.makeText(getActivity(), "Response is Null", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<StoreResponse> call, Throwable t) {

            }
        });
    }

}
