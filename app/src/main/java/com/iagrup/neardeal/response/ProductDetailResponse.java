
package com.iagrup.neardeal.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductDetailResponse {

    @SerializedName("productdetails")
    @Expose
    private Productdetail productdetails = null;
    @SerializedName("success")
    @Expose
    private Boolean success;

    public Productdetail getProductdetails() {
        return productdetails;
    }

    public void setProductdetails(Productdetail productdetails) {
        this.productdetails = productdetails;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

}
